/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf;

import xyz.keahie.jutils.convert.Numeric;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterManager;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.character.characters.Werewolf;
import xyz.keahie.werewolf.event.EventManager;
import xyz.keahie.werewolf.event.events.DayEvent;
import xyz.keahie.werewolf.event.events.NightEvent;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.player.PlayerManager;
import xyz.keahie.werewolf.utils.GameUtils;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameManager {

    private CharacterManager characterManager;
    private PlayerManager playerManager;
    private EventManager eventManager;

    private List<Player> innocentPlayer, werewolfsPlayer;
    private HashMap<Player, String> protectedPlayer;

    private int playerCount, characterCount, werewolfCount, innocentCount,
                round;

    public GameManager() {
        this.characterManager = new CharacterManager();
        this.playerManager = new PlayerManager(this);
        this.eventManager = new EventManager();

        this.innocentPlayer = new ArrayList<>();
        this.werewolfsPlayer = new ArrayList<>();
        this.protectedPlayer = new HashMap<>();
    }

    public void menu() {
        GameUtils.characterManager = characterManager;
        GameUtils.playerManager = playerManager;

        System.out.println(
                "\n" +
                "**********************\n" +
                "*  1. Spiel starten  *\n" +
                "*  2. Spiel beenden  *\n" +
                "**********************");
        int choice = Numeric.stringToInt("Eingabe: ", "Ungültige Auswahl", 2, 1);

        switch (choice) {

            case 1:
                startMenu();
                break;
            case 2:
                System.out.println("Auf Wiedersehen");
                break;

        }
    }

    private void startMenu() {
        System.out.println(
                "\n" +
                "***************************\n" +
                "*   Wie möchtest du die   *\n" +
                "*    Spieler einlesen?    *\n" +
                "*                         *\n" +
                "*        1. Eingabe       *\n" +
                "*         2. Datei        *\n" +
                "*         3. Debug        *\n" +
                "* 4. Zurück zum Hauptmenü *\n" +
                "***************************");
        int choice = Numeric.stringToInt("Eingabe: ", "Ungültige Auswahl", 4, 1);

        switch (choice) {

            case 1:
                start(false, false);
                break;
            case 2:
                start(true, false);
                startMenu();
                break;
            case 3:
                start(false, true);
            case 4:
                menu();
                break;

        }
    }

    /**
     * Wird ganz zu Beginn eines Spieles ausgeführt. Initialisiert alles wichtige
     */
    private void start(boolean fileInput, boolean debug) {
        this.round = 0;
        PrintUtils.gameStart();

        if (!debug) {
            if (fileInput) {
                if (!GameUtils.readPlayersFromFile()) {
                    menu();
                    return;
                }
                playerCount = playerManager.getPlayers().size();
                if (!GameUtils.doesAllPlayersHaveACharacter()) {
                    PrintUtils.facilityWhichCharacter();
                    characterManager.setActiveCharacters(GameUtils.pickActiveCharacters());
                    int maxWerewolf = (playerCount - characterManager.getActiveCharacters().size()) / 2;
                    werewolfCount = Numeric.stringToInt(PrintUtils.facilityHowManyWerewolfs(maxWerewolf),
                            "Ungültige Werwolf-Eingabe!", maxWerewolf, 1);
                } else {
                    for (Player player : playerManager.getPlayers()) {
                        if (player.getCharacter().getCharacterType() == CharacterType.INNOCENT) {
                            innocentPlayer.add(player);
                        } else if (player.getCharacter().getCharacterType() == CharacterType.SPECIAL) {
                            characterManager.addActiveCharacter(player.getCharacter());
                            innocentPlayer.add(player);
                        } else if (player.getCharacter().getCharacterType() == CharacterType.EVIL) {
                            werewolfsPlayer.add(player);
                        }
                    }
                }
            } else {
                PrintUtils.facilitiyHowManyPlayer();
                playerCount = Numeric.stringToInt("Eingabe: ", "Ungültige Spieler-Eingabe!");
                PrintUtils.facilityWhichCharacter();
                characterManager.setActiveCharacters(GameUtils.pickActiveCharacters());
                GameUtils.askForNames();
                int maxWerewolf = (playerCount - characterManager.getActiveCharacters().size()) / 2;
                werewolfCount = Numeric.stringToInt(PrintUtils.facilityHowManyWerewolfs(maxWerewolf),
                        "Ungültige Werwolf-Eingabe!", maxWerewolf, 1);
            }
        } else {
            playerCount = Numeric.stringToInt("Spieler Anzahl: ", "Ungültige Eingabe", Integer.MAX_VALUE, 5);
            PrintUtils.facilityWhichCharacter();
            characterManager.setActiveCharacters(GameUtils.pickActiveCharacters());
            GameUtils.createPlayersForDebug();
            werewolfCount = (playerCount - characterManager.getActiveCharacters().size()) / 2;
        }

        innocentCount = playerCount - werewolfCount;
        characterCount = characterManager.getSpecialCharacters().size();
        if (!GameUtils.doesAllPlayersHaveACharacter()) {
            GameUtils.handOutCards();
        }
        PrintUtils.printAllPlayers();
        run();
    }

    /**
     * Hier wird der Tag/Nacht Rythmus geregelt
     */
    private void run() {
        while (true) {

            onNight();
            if (!onDay()) {
                break;
            }
        }
    }

    /**
     * Alles was in der Nacht passiert
     */
    private void onNight() {
        PrintUtils.itsNight();
        if (round == 0) {
            Character character = GameUtils.getCharacter("Thief");
            if (characterManager.getActiveCharacters().contains(character)) {
                Player player = GameUtils.getPlayerFromCharacter(innocentPlayer, character);
                if (player != null) {
                    player.getCharacter().onNight(player);
                }
            }
            character = GameUtils.getCharacter("Amor");
            if (characterManager.getActiveCharacters().contains(character)) {
                Player player = GameUtils.getPlayerFromCharacter(innocentPlayer, character);
                if (player != null) {
                    player.getCharacter().onNight(player);
                }
            }
        }
        round++;
        for (Player player : innocentPlayer) {
            Character character = player.getCharacter();
            if (characterManager.getActiveCharacters().contains(character)) {
                character.onNight(player);
            }
        }

        new Werewolf().onNight(null);

        eventManager.runEvent(new NightEvent());
    }

    /**
     *
     * Alles was am Tag passiert
     *
     * @return True falls das Spiel weitergehen soll. False falls das Spiel beendet ist
     */
    private boolean onDay() {
        PrintUtils.itsDay();
        for (Player player : playerManager.getTempDeathPlayer()) {
            if (!protectedPlayer.containsKey(player)) {
                playerManager.killPlayer(player);
            } else {
                System.out.println(protectedPlayer.get(player));
            }
        }
        protectedPlayer.clear();
        if (GameUtils.checkIfGameIsOver()) {
            return false;
        }
        playerManager.getTempDeathPlayer().clear();
        PrintUtils.day();
        eventManager.runEvent(new DayEvent());
        Player player = GameUtils.choosePlayer();
        playerManager.killPlayer(player);
        return !GameUtils.checkIfGameIsOver();
    }

    public List<Character> getSpecialCharacters() {
        return characterManager.getSpecialCharacters();
    }

    public List<Character> getWerewolfCharacters() {
        return characterManager.getWerewolfCharacters();
    }

    public CharacterManager getCharacterManager() {
        return characterManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public List<Player> getInnocentPlayer() {
        return innocentPlayer;
    }

    public void setInnocentPlayer(List<Player> innocentPlayer) {
        this.innocentPlayer = innocentPlayer;
    }

    public List<Player> getWerewolfsPlayer() {
        return werewolfsPlayer;
    }

    public void setWerewolfsPlayer(List<Player> werewolfsPlayer) {
        this.werewolfsPlayer = werewolfsPlayer;
    }

    public HashMap<Player, String> getProtectedPlayer() {
        return protectedPlayer;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public int getCharacterCount() {
        return characterCount;
    }

    public void setCharacterCount(int characterCount) {
        this.characterCount = characterCount;
    }

    public int getWerewolfCount() {
        return werewolfCount;
    }

    public void setWerewolfCount(int werewolfCount) {
        this.werewolfCount = werewolfCount;
    }

    public int getInnocentCount() {
        return innocentCount;
    }

    public void setInnocentCount(int innocentCount) {
        this.innocentCount = innocentCount;
    }

    public int getRound() {
        return round;
    }
}
