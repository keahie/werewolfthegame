/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character;

import xyz.keahie.werewolf.utils.ClassUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class CharacterManager {

    private List<Character> characters = new ArrayList<>();
    private List<Character> specialCharacters = new ArrayList<>();
    private List<Character> werewolfCharacters = new ArrayList<>();
    private List<Character> activeCharacters = new ArrayList<>();

    /**
     * Fügt alle Rollen zu den richtigen Listen hinzu + holt sich die Rollen-Klassen automatisch aus dem Package
     */
    public CharacterManager() {
        try {
            Class[] classes = ClassUtils.getClasses("xyz.keahie.werewolf.character.characters");
            for (Class clazz : classes) {

                Constructor<?> constructor = clazz.getConstructor();
                Character character = (Character) constructor.newInstance();

                switch (character.getCharacterType()) {

                    case INNOCENT:
                        addCharacter(character);
                        break;
                    case SPECIAL:
                        addSpecialCharacter(character);
                        break;
                    case EVIL:
                        addWerewolfCharacter(character);
                        break;
                    default:
                        System.err.println("Something went wrong!");
                }
           }
        } catch (ClassNotFoundException | IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void addCharacter(Character character) {
        this.characters.add(character);
    }

    private void addSpecialCharacter(Character character) {
        this.specialCharacters.add(character);
        this.characters.add(character);
    }

    private void addWerewolfCharacter(Character character) {
        this.werewolfCharacters.add(character);
        this.characters.add(character);
    }

    public void setActiveCharacters(List<Character> activeCharacters) {
        this.activeCharacters = activeCharacters;
    }

    public void addActiveCharacter(Character character) {
        this.activeCharacters.add(character);
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public List<Character> getSpecialCharacters() {
        return specialCharacters;
    }

    public List<Character> getWerewolfCharacters() {
        return werewolfCharacters;
    }

    public List<Character> getActiveCharacters() {
        return activeCharacters;
    }
}
