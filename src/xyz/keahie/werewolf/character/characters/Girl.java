/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characters;

import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.player.Player;

public class Girl extends Character{

    public Girl() {
        super(CharacterType.SPECIAL);
    }

    @Override
    public void onNight(Player player) {

    }

    @Override
    public void onDeath(Player player) {

    }
}
