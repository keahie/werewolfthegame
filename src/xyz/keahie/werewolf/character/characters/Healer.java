/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characters;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.GameUtils;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.List;

public class Healer extends Character {

    private static Player lastHealed;

    public Healer() {
        super(CharacterType.SPECIAL);
        lastHealed = null;
    }

    @Override
    public void onNight(Player player) {
        PrintUtils.healerWakesUp();
        List<Player> list = GameUtils.getAllPlayersWithoutSpecificPlayer(lastHealed);
        lastHealed = GameUtils.choosePlayer(list);
        PrintUtils.healerHealed(lastHealed);
        Game.gameManager.getProtectedPlayer().put(lastHealed, PrintUtils.healerSuccessfullHealed(lastHealed));
    }

    @Override
    public void onDeath(Player player) {
        lastHealed = null;
    }
}
