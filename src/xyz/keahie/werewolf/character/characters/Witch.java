/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characters;

import xyz.keahie.jutils.convert.Numeric;
import xyz.keahie.jutils.convert.Texting;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.character.characterUtils.witch.Potion;
import xyz.keahie.werewolf.character.characterUtils.witch.PotionManager;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.ArrayList;
import java.util.List;

public class Witch extends Character {

    private PotionManager potionManager;

    public Witch() {
        super(CharacterType.SPECIAL);
        this.potionManager = new PotionManager();
    }

    @Override
    public void onNight(Player player) {

        if (potionManager.potionsAvailable()) {
            PrintUtils.witchWakesUp();
            List<String> list = new ArrayList<>();
            list.add("ja");
            list.add("nein");

            String input = Texting.getUserInput("Eingabe: ", "Du musst entweder 'ja' oder 'nein' eingeben!", list);
            if (input.equals("ja")) {
                System.out.println();
                List<Potion> potions = potionManager.getPotions();
                PrintUtils.printElementsFromListWithNumber(potions);
                Potion potion = potions.get(Numeric.stringToInt("Eingabe: ", "Ungültige Potion-Eingabe", potions.size(), 1) - 1);
                potionManager.usePotion(potion);

            }
        }
    }

    @Override
    public void onDeath(Player player) {

    }
}
