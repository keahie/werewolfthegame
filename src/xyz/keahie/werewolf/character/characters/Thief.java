/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characters;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.GameUtils;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static xyz.keahie.werewolf.utils.GameUtils.*;

public class Thief extends Character {

    public static List<Character> characters;

    public Thief() {
        super(CharacterType.SPECIAL);
        characters = new ArrayList<>();
    }

    public static int handOutCards(List<Player> players, List<Character> characterList, int werewolfCount) {
        Thief thief = new Thief();
        if (isCharacterInList(characterList, thief)) {
            Random random = new Random();
            int choice = random.nextInt(players.size());
            Player player = players.get(choice);
            player.setCharacter(thief);

            characterList.remove(getCharacterPosition(characterList, player.getCharacter()));
            for (int i = 0; i < 2; i++) {
                random = new Random();
                int x = random.nextInt(2);
                Character character;
                if (x == 1 && characterList.size() != 0) {
                    character = characterList.get(random.nextInt(characterList.size()));
                    characterList.remove(character);
                } else {
                    x = random.nextInt(2);
                    character = x == 1 ? new Villager() : new Werewolf();
                    if (x == 0) {
                        werewolfCount--;
                        if (werewolfCount < 0) {
                            werewolfCount = 0;
                        }
                    }
                }
                characters.add(character);
            }

            if (isListFullOneCharacter(characters, "Werewolf")) {
                characters.remove(0);
            }
        }
        return werewolfCount;
    }

    public void changeCharacter(List<Character> list, Player player) {
        player.setCharacter(pickCard(list));
    }

    private Character pickCard(List<Character> list) {
        return GameUtils.chooseCharacter(list);
    }

    @Override
    public void onNight(Player player) {
        if (Game.gameManager.getRound() == 0) {
            PrintUtils.thiefWakesUp();
            changeCharacter(characters, player);
        }
    }

    @Override
    public void onDeath(Player player) {

    }
}
