/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characters;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.GameUtils;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.ArrayList;
import java.util.List;

public class Amor extends Character {

    private static List<Player> lovePlayer;

    public Amor() {
        super(CharacterType.SPECIAL);
        lovePlayer = new ArrayList<>();
    }

    public static void checkLovePlayers(Player player) {
        if (lovePlayer.contains(player) && lovePlayer.size() == 2) {
            lovePlayer.remove(player);
            PrintUtils.amorLoveDeath();
            Game.gameManager.getPlayerManager().killPlayer(lovePlayer.get(0));
        }
    }

    @Override
    public void onNight(Player player) {
        if (Game.gameManager.getRound() == 0) {
            PrintUtils.amorBegin();
            List<Player> list = GameUtils.getAllPlayersWithoutSpecificCharacter("Amor");
            Player player1 = GameUtils.choosePlayer(list);
            list.remove(player1);
            Player player2 = GameUtils.choosePlayer(list);
            lovePlayer.add(player1);
            lovePlayer.add(player2);
        }
    }

    @Override
    public void onDeath(Player player) {

    }
}
