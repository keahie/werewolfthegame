/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character;

import xyz.keahie.werewolf.player.Player;

public abstract class Character {

    private String name;
    private CharacterType characterType;

    public Character(CharacterType characterType) {
        setName();
        this.characterType = characterType;
    }

    /**
     * Wird aufgerufen, sobald die Rolle in der Nacht an der Reihe ist
     *
     * @param player Der Spieler welche die Methode aufruft
     */
    public abstract void onNight(Player player);

    /**
     *
     * Wird beim Tot eines Spielers mit dieser Rolle aufgerufen
     *
     * @param player Der Spieler welcher gestorben ist
     */
    public abstract void onDeath(Player player);

    public String getName() {
        return name;
    }

    private void setName() {
        this.name = getClass().getSimpleName();
    }

    public CharacterType getCharacterType() {
        return characterType;
    }

    @Override
    public String toString() {
        return name;
    }
}
