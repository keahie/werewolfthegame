
/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characterUtils.witch;

import xyz.keahie.werewolf.player.Player;

public abstract class Potion {

    private String name;

    public Potion() {
        setName();
    }

    public abstract void onUse(Player player);

    private void setName() {
        this.name = getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return name;
    }
}
