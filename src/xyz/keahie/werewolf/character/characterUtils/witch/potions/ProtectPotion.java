/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characterUtils.witch.potions;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.character.characterUtils.witch.Potion;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.PrintUtils;

public class ProtectPotion extends Potion {

    @Override
    public void onUse(Player player) {
        Game.gameManager.getProtectedPlayer().put(player, PrintUtils.witchProtectPotion(player));
    }
}
