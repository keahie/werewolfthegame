
/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.character.characterUtils.witch;

import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.ClassUtils;
import xyz.keahie.werewolf.utils.GameUtils;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class PotionManager {

    private List<Potion> potions = new ArrayList<>();

    public PotionManager() {
        try {
            Class[] classes = ClassUtils.getClasses("xyz.keahie.werewolf.character.characterUtils.witch.potions");
            for (Class clazz : classes) {

                Constructor<?> constructor = clazz.getConstructor();
                Potion potion = (Potion) constructor.newInstance();
                addPotion(potion);

            }
        } catch (ClassNotFoundException | IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void addPotion(Potion potion) {
        this.potions.add(potion);
    }

    public void usePotion(Potion potion) {
        PrintUtils.witchPlayerChoose();
        Player player = GameUtils.choosePlayer();
        potion.onUse(player);
        potions.remove(potion);
        PrintUtils.witchPotionUsed(potion, player);
    }

    public boolean potionsAvailable() {
        return !potions.isEmpty();
    }

    public List<Potion> getPotions() {
        return potions;
    }
}
