/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.player;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.GameManager;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.character.characters.Amor;
import xyz.keahie.werewolf.event.events.player.KillEvent;
import xyz.keahie.werewolf.utils.PrintUtils;

import java.util.ArrayList;
import java.util.List;

public class PlayerManager {

    private List<Player> players;
    private List<Player> tempDeathPlayer;

    private GameManager gameManager;

    public PlayerManager(GameManager gameManager) {
        this.players = new ArrayList<>();
        this.tempDeathPlayer = new ArrayList<>();
        this.gameManager = gameManager;
    }

    public void addPlayer(String name) {
        this.players.add(new Player(name, null, null));
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void addTempDeathPlayer(Player player) {
        this.tempDeathPlayer.add(player);
    }

    /**
     *
     * Tötet den angegebenen Spieler und ruft das "onDeath" Event für die jeweilige Rolle auf
     *
     * @param player Der Spieler welcher umgebracht werden soll
     */
    public void killPlayer(Player player) {
        players.remove(player);

        if (!(player.getCharacter().getCharacterType() == CharacterType.EVIL)) {
            gameManager.setInnocentCount(gameManager.getInnocentCount() - 1);
            gameManager.getInnocentPlayer().remove(player);
            if (!(player.getCharacter().getCharacterType() == CharacterType.INNOCENT)) {
                gameManager.setCharacterCount(gameManager.getCharacterCount() - 1);
                if (gameManager.getCharacterManager().getActiveCharacters().contains(player.getCharacter())) {
                    gameManager.getCharacterManager().getActiveCharacters().remove(player.getCharacter());
                }
            }
        } else {
            gameManager.setWerewolfCount(gameManager.getWerewolfCount() - 1);
            gameManager.getWerewolfsPlayer().remove(player);
        }
        gameManager.setPlayerCount(gameManager.getPlayerCount() - 1);
        PrintUtils.playerKilled(player);
        player.getCharacter().onDeath(player);
        Amor.checkLovePlayers(player);
        Game.gameManager.getEventManager().runEvent(new KillEvent(player));
    }

    public Player getPlayer(int id) {
        for (int i = 0; i < players.size(); i++) {
            if (i == id) {
                return players.get(i);
            }
        }
        return null;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Player> getTempDeathPlayer() {
        return tempDeathPlayer;
    }
}
