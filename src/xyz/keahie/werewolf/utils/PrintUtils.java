/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.utils;

import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.character.CharacterManager;
import xyz.keahie.werewolf.character.characterUtils.witch.Potion;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.player.PlayerManager;

import java.util.List;

public class PrintUtils {

    private static CharacterManager characterManager = Game.gameManager.getCharacterManager();
    private static PlayerManager playerManager = Game.gameManager.getPlayerManager();

    public static void gameStart() {
        System.out.println("\nWillkommen bei dem Spiel Werwölfe von Düsterwald!\n" +
                "Als erstes beginnen wir mit der Einrichtung ...");
    }

    public static void itsNight() {
        System.out.println("\nEs ist Nacht");
    }

    public static void itsDay() {
        System.out.println("\nEs ist Tag");
    }

    public static void facilitiyHowManyPlayer() {
        System.out.println("Wie viele Spieler spielen mit?");
    }

    public static void facilityWhichCharacter() {
        System.out.println("\nMit welchen Charakteren möchtest du spielen (0 für alle | -1 zum Beenden): ");
    }

    public static String facilityHowManyWerewolfs(int maxWerewolf) {
        return "\nWie viele Werwölfe spielen mit (max " + maxWerewolf + "): ";
    }

    public static void day() {
        System.out.println("\nAbstimmung ...");
        System.out.println("Wer soll getötet werden?");
    }

    public static void werewolfsWakesUp() {
        System.out.println("\nDie Werwölfe erwachen. Wen werden sie sich schnappen?");
    }

    public static void seerWakesUp() {
        System.out.println("\nDie Seherin erwacht und sucht sich eine Person aus");
    }

    public static void seerChoose(Player player) {
        System.out.println("\nDer Spieler " + player + " ist " + (player.getCharacter().getName().equals("Werewolf") ? "ein" : "kein") + " Werwolf!");
    }

    public static void hunterDeath() {
        System.out.println("\nDer Jäger ist tot. Wen möchte der Jäger erschiessen: ");
    }

    public static void amorBegin() {
        System.out.println("\nWelche beiden Spieler sollen ein Paar werden:");
    }

    public static void amorLoveDeath() {
        System.out.println("\nEiner der liebenden ist gestorben. Daraufhin stirbt auch der andere");
    }

    public static void healerWakesUp() {
        System.out.println("\nDer Heiler erwacht. Er wird nun einen Spieler vor den Wölfen beschützten:");
    }

    public static void healerHealed(Player player) {
        System.out.println("\nDer Heiler schützt " + player + " für eine Nacht");
    }

    public static String healerSuccessfullHealed(Player player) {
        return "\nDer Heiler hat erfolgreich " + player + " beschützt";
    }

    public static void witchWakesUp() {
        System.out.println("\nDie Hexe erwacht. Wird sie einen ihrer magischen Tränke einsetzten (ja/nein)?");
    }

    public static void witchPlayerChoose() {
        System.out.println("\nAuf welchen Spieler wird die Hexe ihren Trank einsetzten:");
    }

    public static void witchPotionUsed(Potion potion, Player player) {
        System.out.println("\nDie Hexe hat eine " + potion + " auf " + player + " eingesetzt!");
    }

    public static String witchProtectPotion(Player player) {
        return "\nDer Heiltrank der Hexe hat " + player + " erflogreich beschützt!";
    }

    public static void thiefWakesUp() {
        System.out.println("\nDer Dieb erwacht und sucht sich eine Karte aus:");
    }

    public static void playerKilled(Player player) {
        System.out.println("\nDer Spieler " + player + " wurde getötet");
    }

    public static void printAllPlayers() {
        for (int i = 0; i < playerManager.getPlayers().size(); i++) {
            Player player = playerManager.getPlayers().get(i);
            System.out.println((i + 1) + ". " + player);
        }
    }

    /**
     *
     * Gibt eine Liste mit allen Elementen aus der übergebenen Liste aus. Zusätzlich ist diese numeriert
     *
     * @param list Die Liste welche ausgegeben werden soll
     */
    public static void printElementsFromListWithNumber(List list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println((i + 1) + ". " + list.get(i));
        }
    }

    public static void printFileNotFound(String name) {
        System.out.println("\nDie Datei \"" + name + "\" wurde nicht gefunden!\n" +
                "Btte stell sicher, dass die Datei in dem selben Ordner ist wie das Spiel!");
    }

    public static void printFileCorrupted(String name) {
        System.out.println("\nDie Datei \"" + name + "\" scheint Fehler zu beinhalten.\n" +
                "Bitte überprüfe ob es in der Datei Fehler gibt!");
    }
}
