/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  *
 *  * All methods which are not marked as '#myself', are not written by myself
 *  * Here is the post from original creator:
 *  * https://stackoverflow.com/questions/520328/can-you-find-all-classes-in-a-package-using-reflection/520344#520344
 *  *
 *  ******************************************************
 */

package xyz.keahie.werewolf.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public class ClassUtils {

    /** #myself
     *
     */
    public static boolean compileJavaFile(String path, String filename) {
        int errorCode = com.sun.tools.javac.Main.compile(new String[] {
                "-cp", "/character_external",
                "-d", "/temp/werewolf_addons",
                filename});
        return errorCode == 0;
    }

    /** #myself
     *
     * Lädt eine neue Klasse aus einer Datei
     *
     * @param folder Der Ordner in welchem sich die Datei befindet
     * @param filename Der Name der Datei
     * @return Eine neue Klasse
     */
    public static Class getClassFromExternalFile(File folder, String filename) throws ClassNotFoundException, FileNotFoundException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        for (File file : folder.listFiles()) {
            if (file.getName().equals(filename + ".java")) {
                if (compileJavaFile(folder.getPath(), file.getName())) {
                    String path = file.getPath().replace("/", ".");
                    return classLoader.loadClass(path);
                }
                break;
            }
        }
        return null;
    }

    /**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages
     * but ony if they are in the package which is given to the method
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     */
    public static Class[] getClassesOnlyInPackage(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<Class> classes = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            File folder = new File(resource.getFile());
            for (File file : Objects.requireNonNull(folder.listFiles())) {
                if (file.isFile() && file.getName().endsWith(".class")) {
                    classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
                }
            }
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }
}
