/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.utils;

import xyz.keahie.jutils.convert.Numeric;
import xyz.keahie.jutils.convert.Texting;
import xyz.keahie.werewolf.Game;
import xyz.keahie.werewolf.GameManager;
import xyz.keahie.werewolf.character.Character;
import xyz.keahie.werewolf.character.CharacterManager;
import xyz.keahie.werewolf.character.CharacterType;
import xyz.keahie.werewolf.character.characters.Thief;
import xyz.keahie.werewolf.character.characters.Villager;
import xyz.keahie.werewolf.character.characters.Werewolf;
import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.player.PlayerManager;
import xyz.keahie.werewolf.player.PlayerType;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class GameUtils {

    public static CharacterManager characterManager = Game.gameManager.getCharacterManager();
    public static PlayerManager playerManager = Game.gameManager.getPlayerManager();

    /**
     * Fragt den Spielleiter nach allen Namen für die Spieler
     */
    public static void askForNames() {
        for (int i = 0; i < Game.gameManager.getPlayerCount(); i++) {
            System.out.print("Wie heißt der " + (i + 1) + ". Spieler: ");
            playerManager.addPlayer(Texting.getUserInput("Ungültiger Name"));
        }
    }

    /**
     * Erstellt automatische Testnamen. Nur fürs Debugging!
     */
    public static void createPlayersForDebug() {
        for (int i = 0; i < Game.gameManager.getPlayerCount(); i++) {
            playerManager.addPlayer("Test_" + i);
        }
    }

    /**
     *
     * Fragt mit welchen Rollen man das Spiel spielen will
     *
     * @return Alle Rollen mit denen gespielt wird
     */
    public static List<Character> pickActiveCharacters() {
        List<Character> characters = new ArrayList<>(characterManager.getActiveCharacters());
        List<Character> specialCharacters = new ArrayList<>(characterManager.getSpecialCharacters());

        while (specialCharacters.size() != 0) {
            PrintUtils.printElementsFromListWithNumber(specialCharacters);
            int pick = Numeric.stringToInt("Eingabe: ", "Ungültige Eingabe!", specialCharacters.size(), -1) - 1;
            System.out.println();
            if (pick == -1) {
                return specialCharacters;
            } else if (pick == -2) {
                break;
            } else {
                moveCharacter(specialCharacters.get(pick), specialCharacters, characters);
            }
        }
        return characters;
    }

    /**
     * Verteilt alle Karten zufällig an alle Spieler
     */
    public static void handOutCards() {
        List<Player> players = new ArrayList<>(Game.gameManager.getPlayerManager().getPlayers());
        List<Player> innocentPlayers = new ArrayList<>(players);
        List<Character> characters = new ArrayList<>(Game.gameManager.getCharacterManager().getActiveCharacters());
        Random random = new Random();

        int werewolfCount = Game.gameManager.getWerewolfCount();
        werewolfCount = Thief.handOutCards(players, characters, werewolfCount);

        for (int i = 0; i != werewolfCount; i++) {
            int choice = random.nextInt(players.size());
            Player player = players.get(choice);
            if (player.getCharacter() == null) {
                player.setCharacter(new Werewolf());
                player.setPlayerType(PlayerType.EVIL);
                Game.gameManager.getWerewolfsPlayer().add(player);
                innocentPlayers.remove(player);
            } else {
                i--;
            }
            players.remove(player);
        }

        for (Character character : characters) {
            boolean successful = false;
            while (!successful) {
                int choice = random.nextInt(players.size());
                Player player = players.get(choice);

                if (player.getCharacter() == null) {
                    innocentPlayers.get(getPlayerPosition(innocentPlayers, player)).setCharacter(character);
                    innocentPlayers.get(getPlayerPosition(innocentPlayers, player)).setPlayerType(PlayerType.SPECIAL);
                    players.remove(player);
                    successful = true;
                }
            }

        }

        for (Player player : players) {
            if (player.getCharacter() == null) {
                innocentPlayers.get(getPlayerPosition(innocentPlayers, player)).setCharacter(new Villager());
                innocentPlayers.get(getPlayerPosition(innocentPlayers, player)).setPlayerType(PlayerType.INNOCENT);
            }
        }

        for (Player player : innocentPlayers) {
            Game.gameManager.getInnocentPlayer().add(player);
        }
    }

    /**
     *
     * Gibt die Position des jeweiligen Spielers in der angegebenen Liste zurück
     *
     * @param players Die Liste in welcher nach dem Spieler gesucht werden soll
     * @param player Der Spieler nach dem gesucht wird
     * @return Die Position des Spielers. Falls dieser nicht gefunden wird, wird eine -1 zurück gegeben
     */
    public static int getPlayerPosition(List<Player> players, Player player) {
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i) == player) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * Gibt den ersten Spieler zurück, welcher den benötigten Charakter besitzt
     *
     * @param players Die Liste in welcher gesucht wird
     * @param character Der Charakter welcher der Spieler benötigt
     * @return Der Spieler welcher den Charakter besitzt
     */
    public static Player getPlayerFromCharacter(List<Player> players, Character character) {
        for (Player player : players) {
            if (player.getCharacter() == character) {
                return player;
            }
        }
        return null;
    }

    /**
     *
     * Gibt die Position des jewiligen Charakter in der angegebenen Liste zurück
     *
     * @param characters Die Liste in welcher nach dem Charakter gesucht werden soll
     * @param character Der Charakter nach dem gesucht wird
     * @return Die Position des Charakter. Falls dieser nicht gefunden wird, wird eine -1 zurück gegeben
     */
    public static int getCharacterPosition(List<Character> characters, Character character) {
        for (int i = 0; i < characters.size(); i++) {
            if (characters.get(i).getName().equalsIgnoreCase(character.getName())) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * Gibt eine Liste mit allen Spielern, außer dem übergebenen Spieler zurück
     *
     * @param player Der Spieler welcher nicht in der Liste vorhanden sein soll
     * @return Eine Liste mit allen Spielern außer dem übergebenem
     */
    public static List<Player> getAllPlayersWithoutSpecificPlayer(Player player) {
        List<Player> players = new ArrayList<>();
        for (Player p : Game.gameManager.getPlayerManager().getPlayers()) {
            if (p != player) {
                players.add(p);
            }
        }
        return players;
    }

    /**
     *
     * Gibt eine Liste mit allen Spielern zurück. Spieler welche die angegebene Rolle besitzten, werden nicht in dieser erscheinenen
     *
     * @param character Die Rolle welche nicht in der Liste vorhanden sein soll
     * @return Eine neue Liste mit allen Spielern, außer denen welche die angegebene Rolle besitzten
     */
    public static List<Player> getAllPlayersWithoutSpecificCharacter(String character) {
        List<Player> players = new ArrayList<>();
        for (Player player : Game.gameManager.getPlayerManager().getPlayers()) {
            if (!player.getCharacter().getName().equals(character)) {
                players.add(player);
            }
        }
        return players;
    }

    /**
     *
     * Gibt alle Spieler in einer Liste aus, aus welcher man einen auswählen kann
     *
     * @return Der Spieler welchen man ausgewählt hat
     */
    public static Player choosePlayer() {
        PrintUtils.printAllPlayers();
        int choice = Numeric.stringToInt("Eingabe: ", "Ungültige Eingabe", playerManager.getPlayers().size(), 1) - 1;
        return playerManager.getPlayer(choice);
    }

    /**
     *
     * Gibt alle Spieler in einer Liste aus, aus welcher man einen auswählen kann. Die übergebenen Spieler werden in dieser Liste nicht erscheinen
     *
     * @param players Die Spieler welche in der Liste nicht erscheinen sollen
     * @return Der Spieler welchen man ausgewählt hat
     */
    public static Player choosePlayer(List<Player> players) {
        PrintUtils.printElementsFromListWithNumber(players);
        int choice = Numeric.stringToInt("Eingabe: ", "Ungültige Eingabe", players.size(), 1) - 1;
        return players.get(choice);
    }

    /**
     *
     * Gibt alle Charaktere in einer Liste aus, aus welcher man einen auswählen kann. Die übergebenen Charaktere werden in dieser Liste nicht erscheinen
     *
     * @param characters Die Charaktere welche in der Liste nicht erscheinen sollen
     * @return Der Charakter welchen man ausgewählt hat
     */
    public static Character chooseCharacter(List<Character> characters) {
        PrintUtils.printElementsFromListWithNumber(characters);
        int choice = Numeric.stringToInt("Eingabe: ", "Ungültige Eingabe", characters.size(), 1) - 1;
        return characters.get(choice);
    }

    /**
     *
     * Überprüft ob sich ein Character in einer Liste befindet
     *
     * @param characters Die Liste welche durchsucht werden soll
     * @param character Der Character welcher in der übergebenen Liste gesucht werden soll
     * @return True falls der Character in der Liste ist, ansonsten false
     */
    public static boolean isCharacterInList(List<Character> characters, Character character) {
        for (Character c : characters) {
            if (c.getName().equals(character.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * Testet ob eine Liste voll von einem bestimmten Charakter ist
     *
     * @param list Die Liste welche überprüft werden soll
     * @param name Der name des Charakter welcher in der Liste vor kommen soll
     * @return True falls die Liste voll mit dem Charakter ist, stattdessen False
     */
    public static boolean isListFullOneCharacter(List<Character> list, String name) {
        for (Character character : list) {
            if (!character.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * Sucht einen Charakter nach seinem Namen und gibt diesen zurück
     *
     * @param name Der Charaktername nach dem gesucht wird
     * @return Der Charakter oder null
     */
    public static Character getCharacter(String name) {
        for (Character character : characterManager.getCharacters()) {
            if (character.getName().equals(name)) {
                return character;
            }
        }
        return null;
    }

    /**
     *
     * Gibt den PlayerType zurück welcher zu dem Charakter passt
     *
     * @param character Der Charakter von welchem man sich den PlayerType herausholen möchte
     * @return Der gefundene PlayerType oder null
     */
    public static PlayerType getPlayerTypeFromCharacter(Character character) {

        switch (character.getCharacterType()) {
            case INNOCENT:
                return PlayerType.INNOCENT;
            case EVIL:
                return PlayerType.EVIL;
            case SPECIAL:
                return PlayerType.SPECIAL;
        }
        return null;
    }

    /**
     *
     * Schaut ob alle Spieler einen Charakter haben
     *
     * @return True falls alle Spieler einen Charakter haben, ansonsten False
     */
    public static boolean doesAllPlayersHaveACharacter() {
        for (Player player : playerManager.getPlayers()) {
            if (player.getCharacter() == null) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * Verschiebt eine Rolle zwischen zwei Listen
     *
     * @param character Die Rolle welche verschoben werden soll
     * @param list1 Die Liste aus welcher die Rolle entfernt wird
     * @param list2 Die Liste in welche die Rolle hinzugefügt wird
     */
    private static void moveCharacter(Character character, List<Character> list1, List<Character> list2) {
        list1.remove(character);
        list2.add(character);
    }

    /**
     *
     * Schaut ob das Spiel zu ende ist
     *
     * @return True falls das Spiel zu Ende ist, andernfalls false
     */
    public static boolean checkIfGameIsOver() {
        if (Game.gameManager.getWerewolfCount() == 0 || Game.gameManager.getInnocentCount() == 0) {
            System.out.println("\n\nDie " + (Game.gameManager.getInnocentCount() == 0 ? "Werwölfe" : "Dorfbewohner") + " haben gewonnen!\n");
            return true;
        }
        return false;
    }

    /**
     *
     * Liest alle Spieler aus einer Datei aus
     *
     * @return True falls die Auslesung erfolgreich war. False falls das File nicht gefunden worden ist, oder ein Fehler aufgetreten ist
     */
    public static boolean readPlayersFromFile() {
        String file = Texting.getUserInput("Eingabe (mit Endung!): ", "Ungültiger Listenname");
        try {

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            int count = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains(" - ")) {
                    String[] parts = line.split(" - ");
                    if (parts.length == 2) {
                        if (playerManager.getPlayers().size() > 0 && playerManager.getPlayers().get(count - 1).getCharacter() == null) {
                            throw new IOException();
                        }
                        Character character = getCharacter(parts[1]);
                        if (character == null) {
                            throw new IOException();
                        }
                        PlayerType playerType = getPlayerTypeFromCharacter(character);
                        if (playerType == null) {
                            throw new IOException();
                        }
                        Player player = new Player(parts[0], character, playerType);
                        playerManager.addPlayer(player);
                    } else {
                        throw new IOException();
                    }
                } else {
                    if (count == 0) {
                        playerManager.addPlayer(line);
                    } else if (playerManager.getPlayers().get(count - 1).getCharacter() == null) {
                        playerManager.addPlayer(line);
                    } else {
                        throw new IOException();
                    }
                }
                count++;
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            PrintUtils.printFileNotFound(file);
            return false;
        } catch (IOException e) {
            PrintUtils.printFileCorrupted(file);
            return false;
        }
        return true;
    }
}
