/*
 * ******************************************************
 *  * Copyright (C) 2018-2018 Keanu Hie <hie.keanu@gmail.com>
 *  *
 *  * This file is part of WerewolfTheGame.
 *  *
 *  * WerewolfTheGame can not be copied and/or distributed without the express
 *  * permission of Keanu Hie
 *  ******************************************************
 */

package xyz.keahie.werewolf.event;

import xyz.keahie.werewolf.player.Player;
import xyz.keahie.werewolf.utils.ClassUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class EventManager {

    private List<Event> events = new ArrayList<>();

    public EventManager() {
        try {
            Class[] classes = ClassUtils.getClassesOnlyInPackage("xyz.keahie.werewolf.event.events");
            for (Class clazz : classes) {

                Constructor<?> constructor = clazz.getConstructor();
                Event event = (Event) constructor.newInstance();
                addEvent(event);
            }

            classes = ClassUtils.getClassesOnlyInPackage("xyz.keahie.werewolf.event.events.player");
            for (Class clazz : classes) {

                Constructor<?> constructor = clazz.getConstructor(Player.class);
                Event event = (Event) constructor.newInstance(new Object[] { null });
                addEvent(event);
            }
        } catch (ClassNotFoundException | IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void addEvent(Event event) {
        this.events.add(event);
    }

    public void runEvent(Event event) {
        for (Event e : events) {
            if (e == event) {
                event.run();
            }
        }
    }
}
